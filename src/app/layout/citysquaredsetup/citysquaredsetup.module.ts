import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';

import { StatModule } from '../../shared/modules/stat/stat.module';
import { CitysquaredsetupRouting } from './citysquaredsetup-routing.module';
import { CitysquaredsetupComponent } from './citysquaredsetup.component';

@NgModule({
    imports: [
        CommonModule,
        CitysquaredsetupRouting,
        MatGridListModule,
        StatModule,
        MatCardModule,
        MatCardModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [CitysquaredsetupComponent]
})
export class CitysquaredsetupModule {}
