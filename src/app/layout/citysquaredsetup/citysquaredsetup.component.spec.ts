import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitysquaredsetupComponent } from './citysquaredsetup.component';

describe('CitysquaredsetupComponent', () => {
  let component: CitysquaredsetupComponent;
  let fixture: ComponentFixture<CitysquaredsetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitysquaredsetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitysquaredsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
