import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CitysquaredsetupComponent } from './citysquaredsetup.component';

const routes: Routes = [
    {
        path: '',
        component: CitysquaredsetupComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CitysquaredsetupRouting {}
